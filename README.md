# Dagtwoot

Code for the @dagtwoot bot on twitter. Includes a status generator and access to some of the basic Twitter API in Python. 

## Preamble

This project has an anti-harassment policy at [CONDUCT.md](CONDUCT.md) based on the [the example policy](http://geekfeminism.wikia.com/wiki/Community_anti-harassment) from the Geek Feminism wiki, created by the Geek Feminism community. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

#### Python 3.6

Navigate to the Python website (https://www.python.org/downloads/) and download and install Python 3.6 (currently here: https://www.python.org/ftp/python/3.6.0/python-3.6.0.exe).

When given the option, if running on windows, allow it to add Python 3.6 to PATH.

### Installing

Open a terminal window and navigate to the directory you cloned the repo, and then run:

```
python -m pip install -r ./requirements.txt
```

This should install all the required modules. You should be able to edit and test Dagtwoot now, although you'll need to deploy it to a Twitter account to test out the tweeting functionality. 

## Deployment

### Twitter

Dagtwoot requires app access to a twitter account to function. Log in to twitter using the account you want Dagtwoot to tweet from, and then go to Twitter's [Application Management](https://apps.twitter.com/) and create a new app. Once you have created the app, you need to give Dagtwoot access to the twitter API to make new tweets. 

* Navigate to your application page, and set the Access level to Read and Write.
* View the Keys and Access Tokens page, and regenerate your Access Token and Token Secret, to enable read and write access to the account. 
* Copy the Consumer Key, Consumer Secret, Access Token, and Access Secret into Dagtwoot's [secrets.ini](secrets.ini)

This should enable Dagtwoot to post tweets on the account. To test this, try running:

```
python dagtwoot.py
```

and it should print a big messy pile of JSON to your terminal. If it gives you an error about an invalid or expired token, then the authentication has been messed up and you should probably find a better tutorial than this. Sorry! <3 

If this works, then your account should have tweeted one of a number of cute doggy tweets! 

### Heroku

Dagtwoot can be deployed to Heroku using the included procfile. Create an account on the heroku website, then create a new app. Navigate to the 'Deploy' tab, and you can follow the instructions there for an existing git repo to push to the heroku app. 

After this, you want to go to the Resources tab, and search for the 'Heroku Scheduler' app. Add a new job, running

```
python dagtwoot.py
```

with an hourly frequency, and your twitter bot should be up and running. 

## Contributing

Please read [CONTRIBUTING.md]() for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Cassandra Fantastic** - *Initial work* - [Casserpillar](https://github.com/Casserpillar)

See also the list of [contributors](https://gitlab.com/casserpillar/dagtwoot/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
