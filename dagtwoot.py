import generators
import pyTweetFunctions
import json
from requests_oauthlib import OAuth1
import configparser

#job scheduler
#job executer

def main():
    auth = getAuth()
    status = generators.makeDagString()
    response = pyTweetFunctions.postStatus(auth, status)
    message = json.loads(response.text)
    print(message)
    return

def getAuth():
    config = configparser.ConfigParser()
    config.read('secrets.ini')
    secrets = config['secrets']
    consumer_key = secrets['consumer_key']
    consumer_secret = secrets['consumer_secret']
    access_token = secrets['access_token']
    access_secret = secrets['access_secret']
    auth = OAuth1(consumer_key, consumer_secret, access_token, access_secret)
    return auth

if __name__ == '__main__':
    main()
