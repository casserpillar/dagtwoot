import requests

# Send message to screen_name
def sendMessageByName(auth, screen_name, text):
	url = 'https://api.twitter.com/1.1/direct_messages/new.json'
	params = {'screen_name': screen_name, 'text': text}
	response = requests.post(url=url, auth=auth, params=params)
	return response

# Send message to user_id
def sendMessageByID(auth, user_id, text):
	url = 'https://api.twitter.com/1.1/direct_messages/new.json'
	params = {'user_id': user_id, 'text': text}
	response = requests.post(url=url, auth=auth, params=params)
	return response

# Get messages since since_ID
def getMessages(auth, since_id='0', skip_status='true', include_entities='false'):
	url = 'https://api.twitter.com/1.1/direct_messages.json'
	params = {'since_id': since_id, 'skip_status': skip_status,'include_entities': include_entities}
	response = requests.get(url=url, auth=auth, params=params)
	return response

# Generic post
def sendPost(auth, url, params):
	response = requests.post(url=url, auth=auth, params=params)
	return response

# Generic get
def sendGet(auth, url, params):
	response = requests.get(url=url, auth=auth, params=params)
	return response

# Tweet status
def postStatus(auth, status, in_reply_to_status_id='0'):
	url = 'https://api.twitter.com/1.1/statuses/update.json'
	params = {'status': status, 'in_reply_to_status_id': in_reply_to_status_id}
	response = requests.post(url, auth=auth, params=params)
	return response
