# -*- coding: UTF-8 -*-
#smoldog
import random

def genAwoo():
    numOoo  = random.randint(1,4)
    awoo = "ᵃʷᵒ" + numOoo * "ᵒ"
    return awoo

def genEmoji():
    return "🐶"

def genBorf():
    numBarps = random.randint(1,3)
    barpString = "ᴮᴬᴿᴾ"
    barps = numBarps * barpString + "﹗"
    return barps

def genPant():
    pantType = random.randint(0,1)
    numPant = random.randint(1,2)   
    pant = " *pant*"    
    if (pantType == 0):
        pantString = numPant * pant
    else:
        pantString = "🐶👅" + numPant * pant
    return pantString

def genSniff():
    numSnuff = random.randint(0,1)
    snuffs = "*snuff snuff" + numSnuff * " snuff" + "*"
    foundIt = random.randint(0,2)
    it = "" 
    if (foundIt == 2):
        numIt = 4
        itType = random.randint(1,numIt)
        itDict = {
            1: genAwoo,
            2: genEmoji,
            3: genBorf,
            4: genPant,
        }
        it = itDict[itType]()
    snuffs = snuffs + " " + it
    return snuffs

def makeDagString():
    numTypes = 5
    tweetType = random.randint(1,numTypes)
    tweetDict = {
        1: genAwoo,
        2: genEmoji,
        3: genBorf,
        4: genPant,
        5: genSniff,
    }
    tweetString = tweetDict[tweetType]()
    return tweetString

if (__name__ == "__main__"):
    for i in range(1,100): 
        print(genSniff())
